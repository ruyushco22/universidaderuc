function eliminarAsignatura(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar la asignatura seleccionada?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {
  $("#nuevo_asignatura").validate({
    rules: {
      "idAsignaturaERUC": {
        required: true
      },
      "nombreAsignaturaERUC": {
        required: true,
        letras: true
      },
      "creditosAsignaturaERUC": {
        required: true
      },
      "fechaInicioAsignaturaERUC": {
        required: true
      },
      "fechaFinalizacionAsignaturaERUC": {
        required: true
      },
      "profesorAsignaturaERUC": {
        required: true,
        letras: true
      },
      "descripcionAsignaturaERUC": {
        required: true
      },
      "totalHorasAsignaturaERUC": {
        required: true
      },
      "idCursoERUC": {
        required: true
      }
    },
    messages: {
      "idAsignaturaERUC": {
        required: ""
      },
      "nombreAsignaturaERUC": {
        required: "Por favor, ingrese un nombre valido"
      },
      "creditosAsignaturaERUC": {
        required: "Por favor, ingrese un credito valida"
      },
      "fechaInicioAsignaturaERUC": {
        required: "Por favor, ingrese una fecha de inicio valida"
      },
      "fechaFinalizacionAsignaturaERUC": {
        required: "Por favor, ingrese una fecha de finalizacion valida"
      },
      "profesorAsignaturaERUC": {
        required: "Por favor, ingrese un profesor valido"
      },
      "descripcionAsignaturaERUC": {
        required: "Por favor, ingrese una description valida"
      },
      "totalHorasAsignaturaERUC": {
        required: "Por favor, ingrese un total de horas valido"
      },
      "idCursoERUC": {
        required: "Por favor, seleccione un curso"
      }
    }
  });

  $.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
  }, "Solo se permiten letras");
  
  $('#tbl_asignatura').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LAS ASIGNATURAS.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
      url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
    }
  });
});
