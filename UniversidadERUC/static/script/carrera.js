function eliminarCarrera(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar al cliente seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}
$(document).ready(function() {

  $("#nuevo_carrera").validate({
    rules: {
      "idCarreraERUC": {
        required: true
      },
      "nombreCarreraERUC": {
        required: true
      },
      "directorCarreraERUC": {
        required: true
      },
      "descripcionCarreraERUC": {
        required: true
      },
      "periodoAcademicoCarrera": {
        required: true
      }
    },
    messages: {
      "idCarreraERUC": {
        required: ""
      },
      "nombreCarreraERUC": {
        required: "Por favor, ingrese un nombre de carra valido"
      },
      "directorCarreraERUC": {
        required: "Por favor, ingrese un nombre de director valido"
      },
      "descripcionCarreraERUC": {
        required: "Por favor, ingrese una descripcion valida"
      },
      "periodoAcademicoCarrera": {
        required: "Por favor, ingrese un periodo academico valido"
      }
    }
  });

  $('#tbl_carrera').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LAS CARRERAS.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
