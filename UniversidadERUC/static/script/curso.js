function eliminarCurso(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el curso seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {
  $("#nuevo_curso").validate({
    rules: {
      "idCursoERUC": {
        required: true
      },
      "nivelCursoERUC": {
        required: true
      },
      "descripcionCursoERUC": {
        required: true
      },
      "aulaCursoERUC": {
        required: true
      },
      "ubicacionCursoERUC": {
        required: true
      },
      "maximoCursoERUC": {
        required: true
      },
      "idCarreraERUC": {
        required: true
      }
    },
    messages: {
      "idCursoERUC": {
        required: ""
      },
      "nivelCursoERUC": {
        required: "Por favor, ingrese un nivel valido"
      },
      "descripcionCursoERUC": {
        required: "Por favor, ingrese una descripcion valida"
      },
      "aulaCursoERUC": {
        required: "Por favor, ingrese una aula valida"
      },
      "ubicacionCursoERUC": {
        required: "Por favor, ingrese una ubicacion valida"
      },
      "maximoCursoERUC": {
        required: "Por favor, ingrese una cantidad valida"
      },
      "idCarreraERUC": {
        required: "Por favor, seleccione un nombre"
      }
    }
  });

  $('#tbl_curso').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS CURSOS.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
      url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
    }
  });
});
