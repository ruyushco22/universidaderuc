from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from .models import CarreraERUC
from .models import CursoERUC
from .models import AsignaturaERUC
# Create your views here.


def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')
        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER,[destinatario], fail_silently = True)
        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/enviar_correo')
    return render(request, 'enviar_correo.html')

def grafico(request):
    asignaturaBdd = AsignaturaERUC.objects.all()
    cursoBdd = CursoERUC.objects.all()
    return render(request, 'grafico.html', {'asignatura': asignaturaBdd,'curso': cursoBdd})


def plantilla(request):
    return render(request,'plantilla.html')

def uni(request):
    return render(request,'uni.html')


def listadoCarrera(request):
    carreraBdd = CarreraERUC.objects.all()
    return render(request, 'listadoCarrera.html', {'carrera': carreraBdd})

def guardarCarrera(request):
    nombreCarreraERUC=request.POST["nombreCarreraERUC"]
    directorCarreraERUC=request.POST["directorCarreraERUC"]
    descripcionCarreraERUC=request.POST["descripcionCarreraERUC"]
    periodoAcademicoCarrera=request.POST["periodoAcademicoCarrera"]
    logoCarreraERUC=request.FILES.get("logoCarreraERUC")
    nuevoCarrera=CarreraERUC.objects.create(nombreCarreraERUC=nombreCarreraERUC,directorCarreraERUC=directorCarreraERUC,descripcionCarreraERUC=descripcionCarreraERUC,periodoAcademicoCarrera=periodoAcademicoCarrera, logoCarreraERUC=logoCarreraERUC)
    messages.success(request, 'Carrera Guardada Exitosamente')
    return redirect('/')

def eliminarCarrera(request,id):
    carreraEliminar=CarreraERUC.objects.get(idCarreraERUC=id)
    carreraEliminar.delete()
    messages.success(request, 'Carrera Eliminada Exitosamente')
    return redirect ('/')
def editarCarrera(request,id):
    carreraEditar=CarreraERUC.objects.get(idCarreraERUC=id)
    return render(request,'editarCarrera.html',{'carrera':carreraEditar})

def actualizarCarrera(request):
    if request.method == 'POST':
        idCarreraERUC=request.POST["idCarreraERUC"]
        nombreCarreraERUC=request.POST["nombreCarreraERUC"]
        directorCarreraERUC=request.POST["directorCarreraERUC"]
        descripcionCarreraERUC=request.POST["descripcionCarreraERUC"]
        periodoAcademicoCarrera=request.POST["periodoAcademicoCarrera"]
        #Insertando datos mediante el ORM de DJANGO
        carreraEditar=CarreraERUC.objects.get(idCarreraERUC=idCarreraERUC)
        carreraEditar.nombreCarreraERUC=nombreCarreraERUC
        carreraEditar.directorCarreraERUC=directorCarreraERUC
        carreraEditar.descripcionCarreraERUC=descripcionCarreraERUC
        carreraEditar.periodoAcademicoCarrera=periodoAcademicoCarrera
        if 'logoCarreraERUC' in request.FILES:
            carreraEditar.logoCarreraERUC = request.FILES['logoCarreraERUC']
        carreraEditar.save()
        messages.success(request, 'Carrera Actualizada Exitosamente')
        return redirect('/')
    else:

        pass


def listadoCurso(request):
    cursoBdd = CursoERUC.objects.all()
    carreraBdd = CarreraERUC.objects.all()
    return render(request, 'listadoCurso.html', {'curso': cursoBdd,'carrera': carreraBdd})
def guardarCurso(request):
    idCarreraERUC=request.POST["idCarreraERUC"]
    carreraSelecionado=CarreraERUC.objects.get(idCarreraERUC=idCarreraERUC)
    nivelCursoERUC=request.POST["nivelCursoERUC"]
    descripcionCursoERUC=request.POST["descripcionCursoERUC"]
    aulaCursoERUC=request.POST["aulaCursoERUC"]
    ubicacionCursoERUC=request.POST["ubicacionCursoERUC"]
    maximoCursoERUC=request.POST["maximoCursoERUC"]
    nuevoCurso=CursoERUC.objects.create(nivelCursoERUC=nivelCursoERUC,descripcionCursoERUC=descripcionCursoERUC,aulaCursoERUC=aulaCursoERUC,ubicacionCursoERUC=ubicacionCursoERUC,maximoCursoERUC=maximoCursoERUC,carreraERUC=carreraSelecionado)
    messages.success(request, 'Curso Guardado Exitosamente')
    return redirect('curso')
def eliminarCurso(request,id):
    cursoEliminar=CursoERUC.objects.get(idCursoERUC=id)
    cursoEliminar.delete()
    messages.success(request, 'Curso Eliminado Exitosamente')
    return redirect ('curso')
def editarCurso(request,id):
    cursoEditar = CursoERUC.objects.get(idCursoERUC=id)
    carreraBdd = CarreraERUC.objects.all()
    return render(request, 'editarCurso.html', {'curso': cursoEditar,'carrera': carreraBdd})

def actualizarCurso(request):
    idCursoERUC=request.POST["idCursoERUC"]
    idCarreraERUC=request.POST["idCarreraERUC"]
    carreraSelecionado=CarreraERUC.objects.get(idCarreraERUC=idCarreraERUC)
    nivelCursoERUC=request.POST["nivelCursoERUC"]
    descripcionCursoERUC=request.POST["descripcionCursoERUC"]
    aulaCursoERUC=request.POST["aulaCursoERUC"]
    ubicacionCursoERUC=request.POST["ubicacionCursoERUC"]
    maximoCursoERUC=request.POST["maximoCursoERUC"]
    #Insertando datos mediante el ORM de DJANGO
    cursoEditar=CursoERUC.objects.get(idCursoERUC=idCursoERUC)
    cursoEditar.carreraERUC=carreraSelecionado
    cursoEditar.nivelCursoERUC=nivelCursoERUC
    cursoEditar.descripcionCursoERUC=descripcionCursoERUC
    cursoEditar.aulaCursoERUC=aulaCursoERUC
    cursoEditar.ubicacionCursoERUC=ubicacionCursoERUC
    cursoEditar.maximoCursoERUC=maximoCursoERUC
    cursoEditar.save()
    messages.success(request,'Curso Actualizado Exitosamente')
    return redirect('curso')

def listadoAsignatura(request):
    asignaturaBdd = AsignaturaERUC.objects.all()
    cursoBdd = CursoERUC.objects.all()
    return render(request, 'listadoAsignatura.html', {'asignatura': asignaturaBdd,'curso': cursoBdd})
def guardarAsignatura(request):
    idCursoERUC=request.POST["idCursoERUC"]
    cursoSelecionado=CursoERUC.objects.get(idCursoERUC=idCursoERUC)
    nombreAsignaturaERUC=request.POST["nombreAsignaturaERUC"]
    creditosAsignaturaERUC=request.POST["creditosAsignaturaERUC"]
    fechaInicioAsignaturaERUC=request.POST["fechaInicioAsignaturaERUC"]
    fechaFinalizacionAsignaturaERUC=request.POST["fechaFinalizacionAsignaturaERUC"]
    profesorAsignaturaERUC=request.POST["profesorAsignaturaERUC"]
    descripcionAsignaturaERUC=request.POST["descripcionAsignaturaERUC"]
    totalHorasAsignaturaERUC=request.POST["totalHorasAsignaturaERUC"]
    silaboAsignaturaERUC=request.FILES.get("silaboAsignaturaERUC")
    nuevoAsignatura=AsignaturaERUC.objects.create(nombreAsignaturaERUC=nombreAsignaturaERUC,creditosAsignaturaERUC=creditosAsignaturaERUC,fechaInicioAsignaturaERUC=fechaInicioAsignaturaERUC,fechaFinalizacionAsignaturaERUC=fechaFinalizacionAsignaturaERUC,profesorAsignaturaERUC=profesorAsignaturaERUC,descripcionAsignaturaERUC=descripcionAsignaturaERUC,totalHorasAsignaturaERUC=totalHorasAsignaturaERUC,silaboAsignaturaERUC=silaboAsignaturaERUC,cursoERUC=cursoSelecionado)
    messages.success(request, 'Asignatura Guardada Exitosamente')
    return redirect('asignatura')
def eliminarAsignatura(request,id):
    asignaturaEliminar=AsignaturaERUC.objects.get(idAsignaturaERUC=id)
    asignaturaEliminar.delete()
    messages.success(request, 'Asignatura Eliminado Exitosamente')
    return redirect ('asignatura')
def editarAsignatura(request,id):
    asignaturaEditar = AsignaturaERUC.objects.get(idAsignaturaERUC=id)
    cursoBdd = CursoERUC.objects.all()
    return render(request, 'editarAsignatura.html', {'asignatura': asignaturaEditar,'curso': cursoBdd})

def actualizarAsignatura(request):
    if request.method == 'POST':
        idAsignaturaERUC = request.POST["idAsignaturaERUC"]
        idCursoERUC = request.POST["idCursoERUC"]
        cursoSelecionado = CursoERUC.objects.get(idCursoERUC=idCursoERUC)
        nombreAsignaturaERUC = request.POST["nombreAsignaturaERUC"]
        creditosAsignaturaERUC = request.POST["creditosAsignaturaERUC"]
        fechaInicioAsignaturaERUC = request.POST["fechaInicioAsignaturaERUC"]
        fechaFinalizacionAsignaturaERUC = request.POST["fechaFinalizacionAsignaturaERUC"]
        profesorAsignaturaERUC = request.POST["profesorAsignaturaERUC"]
        descripcionAsignaturaERUC = request.POST["descripcionAsignaturaERUC"]
        totalHorasAsignaturaERUC = request.POST["totalHorasAsignaturaERUC"]

        asignaturaEditar = AsignaturaERUC.objects.get(idAsignaturaERUC=idAsignaturaERUC)
        asignaturaEditar.cursoERUC = cursoSelecionado
        asignaturaEditar.nombreAsignaturaERUC = nombreAsignaturaERUC
        asignaturaEditar.creditosAsignaturaERUC = creditosAsignaturaERUC
        asignaturaEditar.fechaInicioAsignaturaERUC = fechaInicioAsignaturaERUC
        asignaturaEditar.fechaFinalizacionAsignaturaERUC = fechaFinalizacionAsignaturaERUC
        asignaturaEditar.profesorAsignaturaERUC = profesorAsignaturaERUC
        asignaturaEditar.descripcionAsignaturaERUC = descripcionAsignaturaERUC
        asignaturaEditar.totalHorasAsignaturaERUC = totalHorasAsignaturaERUC

        # Manejo de la nueva imagen
        if 'silaboAsignaturaERUC' in request.FILES:
            asignaturaEditar.silaboAsignaturaERUC = request.FILES['silaboAsignaturaERUC']

        asignaturaEditar.save()

        messages.success(request, 'Asignatura Actualizada Exitosamente')
        return redirect('asignatura')
    else:
        # Lógica para manejar casos donde la solicitud no es POST
        # Puedes agregar un mensaje de error o redireccionar según tu necesidad
        pass
