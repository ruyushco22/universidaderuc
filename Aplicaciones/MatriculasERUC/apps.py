from django.apps import AppConfig


class MatriculaserucConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.MatriculasERUC'
