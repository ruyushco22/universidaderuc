from django.urls import path
from . import views
urlpatterns=[

    path('enviar_correo/', views.enviar_correo, name='enviar_correo' ),

    path('grafico',views.grafico, name="grafico"),

    path('',views.listadoCarrera),
    path('guardarCarrera/',views.guardarCarrera),
    path('eliminarCarrera/<id>',views.eliminarCarrera),
    path('editarCarrera/<id>',views.editarCarrera),
    path('actualizarCarrera/', views.actualizarCarrera),

    path('curso',views.listadoCurso, name="curso"),
    path('guardarCurso/',views.guardarCurso),
    path('eliminarCurso/<id>',views.eliminarCurso),
    path('editarCurso/<id>',views.editarCurso),
    path('actualizarCurso/', views.actualizarCurso),

    path('asignatura',views.listadoAsignatura, name="asignatura"),
    path('guardarAsignatura/',views.guardarAsignatura),
    path('eliminarAsignatura/<id>',views.eliminarAsignatura),
    path('editarAsignatura/<id>',views.editarAsignatura),
    path('actualizarAsignatura/', views.actualizarAsignatura)
]
