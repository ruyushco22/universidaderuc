# Generated by Django 5.0.1 on 2024-01-27 02:53

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CarreraERUC',
            fields=[
                ('idCarreraERUC', models.AutoField(primary_key=True, serialize=False)),
                ('nombreCarreraERUC', models.CharField(max_length=150)),
                ('directorCarreraERUC', models.CharField(max_length=150)),
                ('descripcionCarreraERUC', models.TextField()),
                ('periodoAcademicoCarrera', models.CharField(max_length=150)),
                ('logoCarreraERUC', models.FileField(blank=True, null=True, upload_to='logo')),
            ],
        ),
        migrations.CreateModel(
            name='CursoERUC',
            fields=[
                ('idCursoERUC', models.AutoField(primary_key=True, serialize=False)),
                ('nivelCursoERUC', models.CharField(max_length=150)),
                ('descripcionCursoERUC', models.TextField()),
                ('aulaCursoERUC', models.CharField(max_length=150)),
                ('ubicacionCursoERUC', models.CharField(max_length=150)),
                ('maximoCursoERUC', models.CharField(max_length=150)),
                ('carreraERUC', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='MatriculasERUC.carreraeruc')),
            ],
        ),
        migrations.CreateModel(
            name='AsignaturaERUC',
            fields=[
                ('idAsignaturaERUC', models.AutoField(primary_key=True, serialize=False)),
                ('nombreAsignaturaERUC', models.CharField(max_length=150)),
                ('creditosAsignaturaERUC', models.CharField(max_length=150)),
                ('fechaInicioAsignaturaERUC', models.DateField()),
                ('fechaFinalizacionAsignaturaERUC', models.DateField()),
                ('profesorAsignaturaERUC', models.CharField(max_length=150)),
                ('descripcionAsignaturaERUC', models.TextField()),
                ('totalHorasAsignaturaERUC', models.CharField(max_length=150)),
                ('silaboAsignaturaERUC', models.FileField(blank=True, null=True, upload_to='sibalo')),
                ('cursoERUC', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='MatriculasERUC.cursoeruc')),
            ],
        ),
    ]
