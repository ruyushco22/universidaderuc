from django.db import models

# Create your models here.
class CarreraERUC(models.Model):
    idCarreraERUC=models.AutoField(primary_key=True)
    nombreCarreraERUC=models.CharField(max_length=150)
    directorCarreraERUC=models.CharField(max_length=150)
    descripcionCarreraERUC=models.TextField()
    periodoAcademicoCarrera=models.CharField(max_length=150)
    logoCarreraERUC=models.FileField(upload_to='logo',null=True,blank=True)

class CursoERUC(models.Model):
    idCursoERUC=models.AutoField(primary_key=True)
    nivelCursoERUC=models.CharField(max_length=150)
    descripcionCursoERUC=models.TextField()
    aulaCursoERUC=models.CharField(max_length=150)
    ubicacionCursoERUC=models.CharField(max_length=150)
    maximoCursoERUC=models.CharField(max_length=150)
    carreraERUC=models.ForeignKey(CarreraERUC,null=True,blank=True,on_delete=models.PROTECT)

class AsignaturaERUC(models.Model):
    idAsignaturaERUC=models.AutoField(primary_key=True)
    nombreAsignaturaERUC=models.CharField(max_length=150)
    creditosAsignaturaERUC=models.CharField(max_length=150)
    fechaInicioAsignaturaERUC=models.DateField()
    fechaFinalizacionAsignaturaERUC=models.DateField()
    profesorAsignaturaERUC=models.CharField(max_length=150)
    descripcionAsignaturaERUC=models.TextField()
    totalHorasAsignaturaERUC=models.CharField(max_length=150)
    silaboAsignaturaERUC=models.FileField(upload_to='sibalo',null=True,blank=True)
    cursoERUC=models.ForeignKey(CursoERUC,null=True,blank=True,on_delete=models.PROTECT)
